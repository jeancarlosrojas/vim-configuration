"""""""""""""""""""""""                                                                                    
" => General
"""""""""""""""""""""""

" Sets how many lines of history VIM has to remember. By show history, insert :history
set history=700

" Set to auto read when a file is changed from the outside
set autoread

" Show partial commands in the last line of the screen
set showcmd

" enable all Python syntax highlighting features
let python_highlight_all = 1 

"Control Version
set undodir=~/.vim/undodir
set undofile


"""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""

" Turn on the WiLd menu
set wildmenu

" Always show current position
set ruler
"set rulerformat=%l,%v

" Height of the command bar
set cmdheight=2

" show line numbers
set number

" position yourself at a location with the cursor
" set mouse=a

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases 
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch


"""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""

" enable syntax highlighting
syntax enable


"""""""""""""""""""""""
" => Status line
"""""""""""""""""""""""

" Always show the status line
set laststatus=2

" show a visual line under the cursor's current line
set cursorline

" when using the >> or << commands, shift lines by 4 spaces
set shiftwidth=4

" indent when moving to the next line while writing code
set autoindent

" set tabs to have 4 spaces
set ts=4

" expand tabs into spaces
set expandtab

" show the matching part of the pair for [] {} and ()
set showmatch